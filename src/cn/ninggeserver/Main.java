package cn.ninggeserver;

import cn.ninggeserver.lib.*;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.LangDataKeys;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.components.BaseComponent;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectFileIndex;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.ui.ComponentContainer;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowAnchor;
import com.intellij.openapi.wm.ToolWindowFactory;
import com.intellij.openapi.wm.ToolWindowManager;
import com.intellij.psi.*;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;
import com.intellij.ui.content.ContentManager;


import javax.swing.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ningge on 2017/4/14.
 */
public class Main extends AnAction{
    private Project project;
    private ArrayList<File> files = new ArrayList<>();
    private ArrayList<ErrorResult> errors = new ArrayList<>();
    public Main() {
        super("Text _Boxes");
    }

    public void actionPerformed(AnActionEvent event) {
        Project project = event.getData(PlatformDataKeys.PROJECT);
        VirtualFile [] virtualFiles = ProjectRootManager.getInstance(project).getContentSourceRoots();
        FileDocumentManager fileDocumentManager = FileDocumentManager.getInstance();

        this.project = project;

        for(VirtualFile virtualFile : virtualFiles) {
            this.go_through_files(virtualFile, fileDocumentManager);
        }


//        for(File e : files) {
//            System.out.println(e.getName() + "is in " + e.getPath());
//        }
//
        Class[] classes = InspectorProvider.getInspectorsClasses();
//
        try {
            for (Class c : classes) {
                Inspector inspector = (Inspector) c.newInstance();
                errors.addAll(Arrays.asList(inspector.execute(files)));
            }
        } catch (InstantiationException e) {
            System.out.println("can't instantiate");
        }
        catch (IllegalAccessException e) {
            System.out.println(e.getMessage());
        }


        if (errors.size() ==  0) {
            System.out.println("everything is ok");
        } else {
            for (ErrorResult e : errors) {
                System.out.println(e);
            }
        }

        errors.add(new ErrorResult("test", "test", "1", "test", "test"));

        this.showMessage(errors);
    }

    private void go_through_files(VirtualFile virtualFile, FileDocumentManager fileDocumentManager) {
        VirtualFile [] virtualFiles = virtualFile.getChildren();

        for(VirtualFile v : virtualFiles) {
            this.go_through_files(v, fileDocumentManager);
        }

        if (virtualFile.getFileType().getName().equals("JAVA")) {
            PsiFile psiFile = PsiManager.getInstance(this.project).findFile(virtualFile);

            files.add(new File(virtualFile.getPath(), virtualFile.getName(), psiFile));
        }

    }

    private void showMessage(ArrayList<ErrorResult> errors) {
        ToolWindow toolWindow = ToolWindowManager.getInstance(project).registerToolWindow("Test",true, ToolWindowAnchor.BOTTOM, true);
        ToolWindowFactory myFactory = new MyToolWindow(errors);
        myFactory.createToolWindowContent(this.project, toolWindow);
        toolWindow.show(new Runnable() {
            @Override
            public void run() {
                Functions.log("test");
            }
        });

    }
}
