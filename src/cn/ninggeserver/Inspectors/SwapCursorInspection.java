package cn.ninggeserver.Inspectors;

import cn.ninggeserver.lib.ErrorResult;
import cn.ninggeserver.lib.File;
import cn.ninggeserver.lib.Functions;
import cn.ninggeserver.lib.Inspector;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.sun.jna.platform.win32.OaIdl;

import java.util.ArrayList;

/**
 * Created by ningge on 2017/4/24.
 */
public class SwapCursorInspection extends Inspector {
    @Override
    public ErrorResult[] execute(ArrayList<File> files) {
        ArrayList<ErrorResult> e = new ArrayList<>();
        for (File  f  : files) {
            deal(f, e);
        }

        return e.toArray(new ErrorResult[0]);
    }

    private void deal (File f, ArrayList<ErrorResult> e) {
        PsiFile psiFile  = f.getContent();

        psiFile.accept(new JavaRecursiveElementVisitor() {
            @Override
            public void visitClass(PsiClass aClass) {
                super.visitClass(aClass);
                if(aClass != null && aClass.getName() != null && !aClass.getName().equals("CursorAdapter")) {
                    PsiClass temp = aClass.getSuperClass();
                    while (!temp.getName().equals("Object")) {
                        if (temp.getName().equals("CursorAdapter")) {
//                            Functions.log("lalala");
                            PsiFile p = aClass.getContainingFile();
                            p.accept(new JavaRecursiveElementVisitor() {
                                @Override
                                public void visitMethodCallExpression(PsiMethodCallExpression expression) {
                                    super.visitMethodCallExpression(expression);
                                    if(expression.getMethodExpression().getText().equals("this.swapCursor")) {
                                        e.add(new ErrorResult("Api Misuses", "use swap cursor may cause memory leak so do you really want changeCursor",expression.getTextOffset() + "", p.getName(), expression.getText()));
                                    }
                                }
                            });
                            break;
                        }
                        temp = temp.getSuperClass();
                    }
                }
            }
        });

    }
}
