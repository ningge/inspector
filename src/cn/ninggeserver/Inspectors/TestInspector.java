package cn.ninggeserver.Inspectors;

import cn.ninggeserver.lib.ErrorResult;
import cn.ninggeserver.lib.File;
import cn.ninggeserver.lib.Functions;
import cn.ninggeserver.lib.Inspector;
import com.intellij.psi.PsiFile;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ningge on 2017/4/20.
 */
public class TestInspector extends Inspector {
    @Override
    public ErrorResult [] execute(ArrayList<File> files) {

        for (File f : files) {
            this.deal(f);
        }
        return new ErrorResult[0];
    }

    private void deal(File f) {
        PsiFile content = f.getContent();

    }
}
