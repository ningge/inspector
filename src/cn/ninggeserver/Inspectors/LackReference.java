package cn.ninggeserver.Inspectors;

import cn.ninggeserver.lib.ErrorResult;
import cn.ninggeserver.lib.File;
import cn.ninggeserver.lib.Functions;
import cn.ninggeserver.lib.Inspector;
import com.intellij.psi.*;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by ningge on 2017/5/12.
 */
public class LackReference extends Inspector {
    @Override
    public ErrorResult[] execute(ArrayList<File> files) {
        ArrayList<ErrorResult > errorResults = new ArrayList<>();
        for (File f: files) {
            this.deal(f, errorResults);
        }
        return errorResults.toArray(new ErrorResult[0]);
    }

    private void deal(File f, ArrayList<ErrorResult> errorResults) {
        PsiFile psiFile = f.getContent();
        final String content = psiFile.getText();
        try {

            OutputStream out = new BufferedOutputStream(new ObjectOutputStream(new FileOutputStream("fileName")));
        }
        catch (Exception e) {

        }

        psiFile.accept(new JavaRecursiveElementVisitor() {
            @Override
            public void visitNewExpression(PsiNewExpression expression) {
                super.visitNewExpression(expression);
                //Functions.log(expression.getText());
            }

            @Override
            public void visitExpression(PsiExpression expression) {
                super.visitExpression(expression);
            }

            @Override
            public void visitMethodCallExpression(PsiMethodCallExpression expression) {
                super.visitMethodCallExpression(expression);
                Functions.log(expression.getText() + " => " + expression.getParent().getText());
                if(expression.getText() != expression.getParent().getText()) {
                   if(expression.getParent().getClass().getName().equals("PsiExpressionStatementImpl")) {
                       if(expression.getText().indexOf("openInputStream") >=0) {
                           errorResults.add(new ErrorResult("Lack Reference", "Leak reference may lead to leak",expression.getTextOffset() + "", psiFile.getName(), expression.getParent().getText()));
                       }
                   }
                }
            }
        });
    }
}
