package cn.ninggeserver.Inspectors;

import cn.ninggeserver.lib.ErrorResult;
import cn.ninggeserver.lib.File;
import cn.ninggeserver.lib.Functions;
import cn.ninggeserver.lib.Inspector;
import com.intellij.psi.*;

import java.util.ArrayList;

/**
 * Created by ningge on 2017/4/24.
 */
public class CursorInspection extends Inspector {
    @Override
    public ErrorResult[] execute(ArrayList<File> files) {
        ArrayList<ErrorResult > errorResults = new ArrayList<>();
        for (File f: files) {
            this.deal(f, errorResults);
        }
        return errorResults.toArray(new ErrorResult[0]);
    }

    private void deal(File f, ArrayList<ErrorResult> errorResults) {
        PsiFile psiFile = f.getContent();
        final String content = psiFile.getText();

        psiFile.accept(new JavaRecursiveElementVisitor() {
            @Override
            public void visitAssignmentExpression(PsiAssignmentExpression expression) {
                super.visitAssignmentExpression(expression);
                String type = "android.database.Cursor";//"java.lang.String";//
                if(expression.getLExpression().getType().getCanonicalText().equals(type)) {
                    String element = expression.getLExpression().getFirstChild().getText();
                    psiFile.accept(new JavaRecursiveElementVisitor() {
                        @Override
                        public void visitIfStatement(PsiIfStatement statement) {
                            super.visitIfStatement(statement);
                            boolean then = false, else_branch = false;
                            if(statement.getCondition().getText().indexOf(element + "." + "moveToFirst(") >= 0 || statement.getCondition().getText().indexOf(element + "." + "getCount(") >= 0 ) {
                                if (statement.getThenBranch().getText().indexOf(element + "." + "close(") >= 0) {
                                    then = true;
                                }

                                if (statement.getElseBranch().getText().indexOf(element + "." + "close(") >= 0) {
                                    else_branch = true;
                                }
                            }

                            if (then == true && else_branch == false) {
                                errorResults.add(new ErrorResult("Api Misuses", "use moveToFirst on if statement may lead to leak",statement.getTextOffset() + "", psiFile.getName(), statement.getText()));
                            }
                            else if (then == false && else_branch == false) {
                                errorResults.add(new ErrorResult("Api Misuses", "use moveToFirst on if statement may lead to leak",statement.getTextOffset() + "", psiFile.getName(), statement.getText()));
                            }
                        }
                    });

                    int offset = expression.getTextOffset();
                    int nextAssign = content.indexOf(element + " = ", offset);
                    int nextClose = content.indexOf(element + ".close(");



                    if(nextClose < 0 || nextAssign < nextClose) {
                        errorResults.add(new ErrorResult("Lost reference", "lost reference without release resource",expression.getTextOffset() + "", psiFile.getName(), expression.getText()));
                    }


                }
            }




        });
    }
}
