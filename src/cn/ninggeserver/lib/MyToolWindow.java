package cn.ninggeserver.lib;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;
import org.jetbrains.annotations.NotNull;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.*;
import com.intellij.ui.content.*;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by ningge on 2017/4/24.
 */
public class MyToolWindow implements ToolWindowFactory{

    private JList errorList;
    private JTextArea detail;
    private JPanel errors;
    private ArrayList<ErrorResult> errorModel;

    public MyToolWindow(ArrayList<ErrorResult> errors) {
        this.errorModel = errors;

    }


    public void createToolWindowContent(Project project, ToolWindow toolWindow) {
        ContentFactory contentFactory = ContentFactory.SERVICE.getInstance();
        this.buildContent();
        Content content = contentFactory.createContent(errors, "", false);
        toolWindow.getContentManager().addContent(content);
    }

    private void buildContent() {
        DefaultListModel<ErrorResult> defaultListModel = new DefaultListModel<>();

        for (ErrorResult e : this.errorModel) {
            defaultListModel.addElement(e);
        }

        this.errorList.setModel(defaultListModel);
        this.errorList.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.errorList.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {
                    int index = errorList.getSelectedIndex();
                    detail.setText(errorModel.get(index).toString());
                }
            }
        });

    }




}
