package cn.ninggeserver.lib;

/**
 * Created by ningge on 2017/4/20.
 */
public class ErrorResult {
    private String type;
    private String message;
    private String position;
    private String filename;
    private String snippet;

    public ErrorResult() {
    }

    public ErrorResult(String type, String message, String position, String filename, String snippet) {
        this.type = type;
        this.message = message;
        this.position = position;
        this.filename = filename;
        this.snippet = snippet;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getSnippet() {
        return snippet;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    @Override
    public String toString() {
        return this.getMessage();
    }
}
