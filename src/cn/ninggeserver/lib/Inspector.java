package cn.ninggeserver.lib;

import cn.ninggeserver.lib.File;

import java.util.ArrayList;

/**
 * Created by ningge on 2017/4/20.
 */
abstract public class Inspector {
    abstract public ErrorResult[] execute(ArrayList<File> files);
}
