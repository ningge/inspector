package cn.ninggeserver.lib;

import com.intellij.psi.PsiFile;

import java.awt.*;

/**
 * Created by ningge on 2017/4/20.
 */
public class File
{
    private String path;
    private String name;
    private PsiFile content;
    private Cursor c;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PsiFile getContent() {
        return content;
    }

    public void setContent(PsiFile content) {
        this.content = content;
    }

    public File(String path, String name, PsiFile content) {
        this.name = name;
        this.path = path;
        this.content = content;
        this.c = null;


    }
}
