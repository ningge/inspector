package cn.ninggeserver;

import cn.ninggeserver.Inspectors.*;


/**
 * Created by ningge on 2017/4/20.
 */
public class InspectorProvider {
    public static Class [] getInspectorsClasses() {
        return new Class[] {TestInspector.class, SwapCursorInspection.class, CursorInspection.class, LackReference.class};
    }
}
